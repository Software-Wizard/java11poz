package pl.sda;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String args[]) {
        nStrong();
    }

    private static void nStrong() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe zakresu!");
        int value = sc.nextInt();
        int bufor=1;
        if(value == 0){
            System.out.println(bufor);
        }
        for (int i = 1; i <= value; i++) {
            bufor = bufor * i;
        }
        System.out.println(bufor);
    }

    private static void fibonacci() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj maximum zakresu!");
        int max = sc.nextInt();
        int I_minus1 = 0;
        int I_minus2 = 0;

        List<Integer> values = new ArrayList<>();
        for (int i = 0; i < max; i++) {
            if(i < 2){
                values.add(i);
                System.out.println(i);
            }else{
                int result = values.get(i-1) + values.get(i-2);
                values.add(result);
                System.out.println(result);
            }
        }
    }

    private static int innerFibonacci(int i) {
        if(i < 2){
            return i;
        }else{
            return innerFibonacci(i-1)+innerFibonacci(i-2);
        }
    }

    private static void hello(String aMessage) {
        System.out.println("Hello " + aMessage);
    }

    private static void findMin() {
        int[] tablica = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12};
        int bufor = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (bufor > tablica[i]) {
                bufor = tablica[i];
            }
        }
        System.out.print(bufor);
    }

    private static void fizzBuzz() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj minimum zakresu!");
        int min = sc.nextInt();
        System.out.println("Podaj maximum zakresu!");
        int max = sc.nextInt();

        for (int i = min; i <= max; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("fizz buzz");
            }
            else if(i % 5 == 0) {
                System.out.println("buzz");
            }
            else if(i % 3 == 0) {
                System.out.println("fizz");
            }else{
                System.out.println(i);
            }
        }
    }
}
